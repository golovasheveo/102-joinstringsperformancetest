package telran;

import telran.tests.performance.JoinStringsPerformance;
import telran.util.JoinStringsImplBuilder;
import telran.util.JoinStringsImplString;

public class mainApp {
    public static void main ( String[] args ) {
        int nRuns    = 1000;
        int nStrings = 1000;

        var stringsBuilderTest =
                new JoinStringsPerformance (
                        "StringBuilder and append" ,
                        nRuns ,
                        nStrings ,
                        new JoinStringsImplBuilder ()
                );

        var stringConcatenationTest =
                new JoinStringsPerformance (
                        "String concatenation" ,
                        nRuns ,
                        nStrings ,
                        new JoinStringsImplString ()
                );

        stringsBuilderTest.run ();
        stringConcatenationTest.run ();
    }
}

