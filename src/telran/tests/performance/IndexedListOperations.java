package telran.tests.performance;

import telran.util.IndexedList;

public class IndexedListOperations extends PerformanceTest {
    static int probGet = 0;
    private final IndexedList<Integer> list;
    static int nNumbers = 0;

    public IndexedListOperations(String testName, int nRuns, IndexedList<Integer> list, int nNumbers) {
        super(testName, nRuns);
        this.list = list;
        setnNumbers(nNumbers);
        insertToList();
    }

    public void insertToList() {
        int bound = nNumbers;
        for (int i = 0; i < bound; i++) {
            list.add(100);
        }
    }
//    private String detectTestName ( IndexedList<Integer> list ) {
//        String testName = null;
//        if (list instanceof Array ) {
//            testName="Array";
//        }
//        if (list instanceof IndexedLinkedList ) {
//            testName="IndexedLinkedList";
//        }
//        return testName;
//    }

    @Override
    protected void runTest () {
        if (nNumbers == 0) {
            return;
        }
        if (getRandomNumber(100) < probGet)
            runGetAtRandomIndex();
        else
            runRemoveAddFirst();
    }

    public void setProb ( int probGet ) {
        IndexedListOperations.probGet = probGet;
    }

    private void setnNumbers(int nNumbers) {
        IndexedListOperations.nNumbers = nNumbers;
    }

    private void runRemoveAddFirst () {
        list.remove ( 0 );
        list.add ( 0 , 100 );
    }

    private void runGetAtRandomIndex () {
        list.get ( getRandomNumber ( nNumbers ) );
        list.get ( getRandomNumber ( nNumbers ) );

    }

    private int getRandomNumber ( int number ) {
        return ( int ) ( Math.random () * number );
    }


}