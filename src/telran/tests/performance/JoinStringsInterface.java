package telran.tests.performance;

public interface JoinStringsInterface {
    String join ( String[] strings , String delimiter );
}
