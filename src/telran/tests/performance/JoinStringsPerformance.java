package telran.tests.performance;
import java.util.stream.IntStream;

public class JoinStringsPerformance extends PerformanceTest {

    private String[] strings;
    private JoinStringsInterface joinStrings;

    public JoinStringsPerformance(String testName, int nRuns, int nStrings, JoinStringsInterface joinStrings) {
        super(testName, nRuns);
        this.joinStrings = joinStrings;
        createArray (nStrings);
    }


    @Override
    protected void runTest() {
        joinStrings.join(strings, " ");
    }

    final String INIT_STRING = "Hello";

    private void createArray ( int nStrings) {
        strings = new String[nStrings];
        IntStream.range ( 0 , nStrings ).forEach ( i -> strings[i] = INIT_STRING );
    }
}