package telran.tests.performance;

import java.util.stream.IntStream;

public abstract class PerformanceTest {
    private String  testName;
    private Integer nRuns;

    public PerformanceTest ( String testName , Integer nRuns ) {
        this.testName = testName;
        this.nRuns = nRuns;
    }

    protected abstract void runTest ();

    public void run () {
        long startTime = System.currentTimeMillis ();
        IntStream.range ( 0 , nRuns ).forEach ( i -> runTest () );
        long endTime = System.currentTimeMillis ();
        System.out.printf ( "Test completed: %s%n" , testName );
        System.out.printf ( "Execution time (Milliseconds): %d%n" , endTime - startTime );
    }
}