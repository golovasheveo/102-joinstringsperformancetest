package telran.tests;

import org.junit.Test;
import telran.util.JoinStringsImplBuilder;
import telran.util.JoinStringsImplString;

import static org.junit.Assert.assertEquals;

public class JoinStringsTests {

        String[] strings   = new String[]{"Hello", "World"};
        String   delimiter = " ";
        String   expected  = "Hello World";

    @Test
        public void testJoinStringsImplString() {
            JoinStringsImplString joinString = new JoinStringsImplString();
            assertEquals(expected, joinString.join(strings, delimiter ));
        }

    @Test
        public void testJoinStringsImplBuilder() {
            JoinStringsImplBuilder joinString = new JoinStringsImplBuilder();
            assertEquals(expected, joinString.join(strings, delimiter ));
        }
    }
