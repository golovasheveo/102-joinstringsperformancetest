package telran;

import telran.tests.performance.IndexedListOperations;
import telran.util.Array;
import telran.util.IndexedLinkedList;

import static java.lang.String.format;
import static java.util.stream.IntStream.iterate;

public class ListOperationsTestAppl {

    public static void main ( String[] args ) {
        iterate ( 0 ,
                test -> test <= 100 ,
                test -> test + 20 ).forEach ( ListOperationsTestAppl :: accept );
    }

    private static void accept ( int test ) {
        System.out.printf ( "Probability:%d%n" , test );
        performanceTest ( test );
        System.out.println ();
    }


    private static void performanceTest ( int probGet ) {
        int nRuns = 100000;
        int nNumbers = 500000;

        var array = new Array<Integer>();

        var arrayObj = new IndexedListOperations(
                format("Array from %d nNumbers %s nRuns",
                        nNumbers,
                        nRuns),
                nRuns,
                array,
                nNumbers
        );


        var indexedLinkedList = new IndexedLinkedList < Integer > ();
        var indexedLinkedListObj = new IndexedListOperations(
                format("IndexedLinkedList from from %d nNumbers %s nRuns",
                        nNumbers,
                        nRuns),
                nRuns,
                indexedLinkedList,
                nNumbers
        );

        indexedLinkedListObj.setProb(probGet);
        arrayObj.setProb(probGet);


//        indexedLinkedListObj.setnNumbers ( nNumbers );
//        arrayObj.setnNumbers ( nNumbers );

//        indexedLinkedListObj.insertToList();
//        arrayObj.insertToList();

        arrayObj.run();
        indexedLinkedListObj.run();
    }
}